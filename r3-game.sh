#!/bin/sh
cd /opt/virtualx/
if uname -m | grep -q 'x86_64'; then
     ./r3-game.x86_64 $@
  else
     ./r3-game.arm64 $@
  fi
